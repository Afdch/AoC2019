import os
import re
from intcode import intcode

rx = re.compile(r'-?\d+')
with open(os.path.join(os.getcwd(), r"input", r"d05.txt")) as f:
    for line in f:
        opc = [int(x) for x in rx.findall(line)]

# part 1
intcd = intcode(opc[:], [1])
intcd.cd()
print(intcd.output_buffer)
# part 2
intcd = intcode(opc[:], [5])
intcd.cd()
print(intcd.output_buffer)