import os
import timeit

with open(os.path.join(os.getcwd(), r"input", r"d06.txt")) as f:
    orbits = {line[1]: line[0] for line in [line.strip().split(')') for line in f]}

def count_parents(key):
    parents = 0
    while orbits [key] != 'COM':
        parents += 1
        key = orbits[key]
    return parents + 1

def test0():
    a = sum(map(count_parents, orbits.keys()))


#part 2
def chart_orbits(orbit):
    lst = {}
    l = -1
    while (orbit := orbits[orbit]) != 'COM':
        lst[orbit] = (l := l + 1)
    return lst
def test1():
    Y_list = chart_orbits('YOU')
    S_list = chart_orbits('SAN')

    a = (min([Y_list[orbit] + S_list[orbit] for orbit in Y_list.keys() & S_list]))

# part 2 again
def chart_orbits2(orbit):
    lst = set()
    while (orbit := orbits[orbit]) != 'COM':
        lst.add(orbit)
    return lst
def test2():
    you_set = chart_orbits2('YOU')
    san_set = chart_orbits2('SAN')
    a = (len(you_set.symmetric_difference(san_set)))

# part 2 yet again
def test3():
    Y_list = {}
    S_list = {}
    Y_orb = 'YOU'
    S_orb = 'SAN'
    l = 0
    while (Y_orb := orbits[Y_orb]) != 'COM':
        Y_list[Y_orb] = l
        l += 1
    l = 0
    while (S_orb := orbits[S_orb]) not in Y_list:
        S_list[S_orb] = l
        l += 1
    else:
        a = (l + Y_list[S_orb])


n = 1000
print(n, 'repetitions')
print(timeit.timeit(test0, number=n)/n*1000, 'ms')