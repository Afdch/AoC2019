import os
import re
import printd

field = {}
things = {}
with open(os.path.join(os.getcwd(), r"input", r"d24.txt")) as f:
    j = 0
    for line in f:
        i = 0
        for char in line:
            if char == '\n':
                continue
            if  char == '#':
                field[(i, j)] = 1
            else:
                field[(i, j)] = 0
            # elif char == '#':
                # field[(i, j)] = 1
            # if char != '#' and char != '.':
            #     things[char] = (i, j)
            i += 1
        j += 1

tileset = {0: '.', 1: '#'}
# region part 1
def tick_part1():
    field_next = {}
    for x, y in field.keys():
        adj = sum([field[(x+dx, y+dy)] for dx, dy in [(1, 0), (0, 1), (-1, 0), (0, -1)] if (x+dx, y+dy) in field.keys()])
        if field[(x, y)] == 0 and (adj == 1 or adj == 2):
            field_next[(x, y)] = 1
        elif field[(x, y)] == 1 and adj != 1:
            field_next[(x, y)] = 0
        else:
            field_next[(x, y)] = field[(x, y)]
    return field_next

def part1():

    states = set()
    while True:
        printd.printd(field, tileset, False)
        i = 0
        print(i)
        summ = 0
        for y in range(5):
            for x in range(5):
                if field[(x, y)] == 1:
                    summ += 2**i
                i += 1
        if summ in states:
            print(summ)
            break
        states.add(summ)
        field = tick_part1()
# endregion

# region part 2
field2 = {(key, 0): field[key] for key in field.keys()}

def tick_part2():
    field_next = {}
    # check if we need to extend the maze
    levels = set([level for _, level in field2.keys()])
    min_level, max_level = min(levels), max(levels)
    min_extend, max_extend = False, False
    for x in range(5):
        for y in range(5):
            if ((x, y), min_level) in field2.keys() and not min_extend:
                if field2[((x, y), min_level)] == 1:
                    [field2.update({((x, y), min_level - 1): 0}) \
                        for x in range(5) for y in range(5)]
                    min_extend = True
            if ((x, y), max_level) in field2.keys() and not max_extend:
                if field2[((x, y), max_level)] == 1:
                    [field2.update({((x, y), max_level + 1): 0}) \
                        for x in range(5) for y in range(5)]
                    max_extend = True

    for (x, y), level in field2.keys():
        if x == 2 and y == 2:
            continue
        adj = []
        summ = 0
        for dx, dy in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
            nx, ny = (x+dx, y+dy)
            # go inside
            if (nx, ny) == (2, 2):
                if dx == 1:
                    adj.extend([((0, yy), level + 1) for yy in range(5)])
                elif dx == -1:
                    adj.extend([((4, yy), level + 1) for yy in range(5)])
                elif dy == 1:
                    adj.extend([((xx, 0), level + 1) for xx in range(5)])
                elif dy == -1:
                    adj.extend([((xx, 4), level + 1) for xx in range(5)])
            # go outside
            elif nx == 5:
                # if ((3, 2), level-1) in field2.keys():
                adj.append(((3, 2), level-1))
            elif nx == -1:
                # if ((1, 2), level-1) in field2.keys():
                adj.append(((1, 2), level-1))
            if ny == 5:
                # if ((2, 3), level-1) in field2.keys():
                adj.append(((2, 3), level-1))
            elif ny == -1:
                # if ((2, 1), level-1) in field2.keys():
                adj.append(((2, 1), level-1))
            # same level
            elif 0 <= nx <=4 and 0 <= ny <= 4:
                adj.append(((nx, ny), level))
        for t in adj:
            if t not in field2.keys():
                field_next[t] = 0
        # for t in adj:
        #     if t in field2.keys():
            else:
                summ += field2[t]

        # summ = sum([field2[tl] for tl in adj])

        if (((x, y), level) not in field2.keys() or field2[(x, y), level] == 0) and (summ == 1 or summ == 2):
            field_next[(x, y), level] = 1
        elif (((x, y), level) not in field2.keys()) or (field2[(x, y), level] == 1 and summ != 1):
            field_next[(x, y), level] = 0
        else:
            field_next[(x, y), level] = field2[(x, y), level] if ((x, y), level) in field2.keys() else 0
    return field_next

    
def part2():
    global field2
    i = 0
    # while True:
    for i in range(201):
        sm = sum([field2[((x, y), l)] for ((x, y), l) in field2.keys() if 0<=x<=4 and 0<=y<=4])
        print(f"\nMinute {i}, {sm}")        
        # i += 1
        # levels = [level for _, level in field2.keys()]
        # for j in range(min(levels), max(levels)+1):
        #     print(f"\nDepth {j}")
        #     printd.printd({key[0]: field2[key] for key in field2.keys() if key[1] == j}, tileset, False)
        field2 = tick_part2()
    else:
        # levels = [level for _, level in field2.keys()]
        # for j in range(min(levels), max(levels)+1):
        #     print(f"\nDepth {j}, {sm}")
        #     printd.printd({key[0]: field2[key] for key in field2.keys() if key[1] == j}, tileset, False)
        print(sm)

        
# endregion

part2()