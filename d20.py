import os
import re
import printd

maze = {}
things = {}
with open(os.path.join(os.getcwd(), r"input", r"d20.txt")) as f:
    j = 0
    for line in f:
        i = 0
        for char in line:
            if char == '\n':
                continue
            maze[(i, j)] = char
            # if char != '#' and char != '.':
            #     things[char] = (i, j)
            i += 1
        j += 1

portals = dict()

pathways = set([key for key in maze.keys() if maze[key] == '.'])

def fix_portals():
    d = dict()
    global portals
    for key in portals.keys():
        if key not in ['AA', 'ZZ']:
            p1, p2 = portals[key]
            d[p1] = p2
            d[p2] = p1
    d['AA'] = portals['AA']
    d['ZZ'] = portals['ZZ']
    portals = d

def tile_neighs(tile, level):
    port = []
    i, j = tile
    if tile in portals:
        # if tile not in outer portal when it's an level 0
        if 5 < i < 120 and 5 < j < 120:
            port = [(portals[tile], level + 1)]
        elif level > 0:
            port = [(portals[tile], level - 1)]
    return [((x, y), level) for x, y in [(i+1,j), (i-1,j), (i,j+1), (i,j-1)] if (x, y) in pathways] + port

def find_portals():
    for key in maze.keys():
        if 65 <= ord(maze[key]) <= 90:
            x, y = key
            dirs = [(1, 0), (0, 1)]
            for dir in dirs:
                nkey = x + dir[0], y + dir[1]
                if nkey in maze and 65 <= ord(maze[nkey]) <= 90:
                    prtl = "".join([maze[key], maze[nkey]])
                    posit = (x + 2*dir[0], y + 2*dir[1])
                    if posit not in maze or maze[posit] != '.':
                        posit = x - dir[0], y - dir[1]
                    if prtl not in portals:
                        portals[prtl] = [posit]
                    else:
                        portals[prtl].append(posit)



def find_path(A, Z):
    frontier = []
    frontier.append((A, 0, 0))
    visited = {}
    visited[A, 0] = 0
    not_found = True
    while len(frontier) > 0:
        frontier.sort(key = lambda x: x[1], reverse=not_found)
        frontier.sort(key = lambda x: x[2])
        pos, dist, lvl = frontier.pop(0)
        if (Z, 0) in visited:
            # pray for the actual result:
            return visited[(Z, 0)]
            not_found = False
            if visited[(Z, 0)] < min([x for _, x, _ in frontier]):
                return visited[(Z, 0)]
            if dist >= visited[(Z, 0)]:
                continue
        for neigh in tile_neighs(pos, lvl):
            if neigh in visited:
                if visited[neigh] <= dist:
                    continue
                else:
                    visited[neigh] = dist
            frontier.append((neigh[0], dist + 1, neigh[1]))
            visited[neigh] = dist + 1
    
    return visited[Z]

# preparation
find_portals()
fix_portals()
# print(portals)
# part 1:
print(find_path(portals['AA'][0], portals['ZZ'][0]))