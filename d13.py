import os
import re
from intcode import intcode
from printd import printd, imgd

rx = re.compile(r'-?\d+')
with open(os.path.join(os.getcwd(), r"input", r"d13.txt")) as f:
    for line in f:
        opc = [int(x) for x in rx.findall(line)]

opc[0] = 2 # cheat mode
game = intcode(opc[:])


while True:

    # wait for input
    a = input()
    b = 0
    if a == 'a':
        b = -1
    elif a == 'd':
        b = 1
    game.input_buffer.append(b)

    # run game
    q = game.cd()
    X = game.output_buffer[0::3]
    Y = game.output_buffer[1::3]
    V = game.output_buffer[2::3]
    lib = {}
    for i in range(len(X)):
        if X[i] != -1:
            lib[X[i], Y[i]] = V[i]
    print(len([x for x in game.output_buffer[2::3] if x == 2]))
    printd(lib)

    if q == False:
        break


