import os
import re
from intcode import intcode
import printd
import numpy as np

rx = re.compile(r'-?\d+')
with open(os.path.join(os.getcwd(), r"input", r"d19.txt")) as f:
    for line in f:
        opc = [int(x) for x in rx.findall(line)]


def part1(i = 0):
    # How many points are affected by the tractor beam 
    # in the 50x50 area closest to the emitter? 
    # the coordinate X=0, Y=0 is directly in front 
    # of the tractor beam emitter, so the drone 
    # control program will always report 1 at that location
    yy = 56
    xx = 75
    beam = {(x, y): 0 for x in range(xx) for y in range(yy)}
    for y in range(i, yy):
        b_beam = False
        for x in range(int(y * 1.38), min((int(y * 1.59), xx-1))+1):
            print(f"\rcoord = {x:>2}x{y:>2}", end = '')
            drone = intcode(opc[:], [x, y])
            drone.cd()
            beam[(x, y)] = drone.output_buffer[0]

    print(' ')
    printd.printd(beam, reverse=False)
    print(sum(list(beam.values())))

answer = []
def part2():
    square = 99
    global answer
    for y in range(1020, 1035):
    # for y in range(0, 50):
        for x in range(int(y * 1.38), (int(y * 1.59))+1):
            print(x, y, end = ': ')
            drone = intcode(opc[:], [x, y])
            drone.cd()
            if drone.output_buffer[0] == 1:
                drone = intcode(opc[:], [x, y + square])
                drone.cd()
                if drone.output_buffer[0] == 1:
                    drone = intcode(opc[:], [x + square, y])
                    drone.cd()
                    if drone.output_buffer[0] == 1:
                        print('True')
                        answer.append((x, y))
                        break
                    else:
                        print('False')
                else:
                    print('False')    
            else:
                print('False')




answer.sort(key = lambda x, y: x + y)
print(answer)
# 15381032 is too high
# 1523 1022 

# part1(44)
part2()
# part1()