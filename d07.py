import os
import re
from intcode import intcode

rx = re.compile(r'-?\d+')
with open(os.path.join(os.getcwd(), r"input", r"d07.txt")) as f:
    for line in f:
        opc = [int(x) for x in rx.findall(line)]


def day7take2(combination):
    amps = [intcode(opc[:], [combination[x]], x) for x in range(5)]
    amps[0].input_buffer.append(0)

    amp_state = [True]*5
    while any(amp_state):
        for amp in amps:
            if amp.cd() == False:
                amp_state[amp.id] = False
            if len(amp.output_buffer) > 0:
                amps[(amp.id + 1) % 5].\
                    input_buffer.extend([amp.output_buffer.pop()])
    else:
        return amps[0].input_buffer[0]

# try every combination
import itertools
print(max([day7take2(comb) for comb in \
            list(itertools.permutations(range(5, 10), 5))]))