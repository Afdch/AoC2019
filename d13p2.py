import os
import re
from intcode import intcode
from printd import printd, imgd

rx = re.compile(r'-?\d+')
with open(os.path.join(os.getcwd(), r"input", r"d13.txt")) as f:
    for line in f:
        opc = [int(x) for x in rx.findall(line)]

opc[0] = 2                  # cheat number 1
opc[1564:1564+42] = [3]*42  # cheat number 2!
game = intcode(opc[:])

tileset = {0: ' ', 1: '█', 2: '■', 3: 'T', 4: 'O'}
os.system('cls')
# an attempt to win the game with input
inp = list("   dddddddddddddddddd aaaad                                      " + \
    "         aa                                                         " +
    "                                                                                " +
    "dddddddddd   a  "
    "                                                                                " +
    "          " + "          " + "aaaaaa      " + "          " + "          " + "          " 
    + "          " + "          " + "          " + "          " + "          " + "          " 
    + "          " + "aa        " + "          " + "          " + "          " + "          " 
    + "          " + "          " + "          " + "          " + "          " + "          " 
    + "dd        " + "          " + "          " + "          " + "          " + "          " 
    + "dd        " + "          " + "          "  
    + "aaaaaaaaaaaaaaaaaaaa" + "aaaaaaaa  "+ "          " + "          " + "          " 
    + "          "  + "          " + "          " + "          " + "          " + "          " + "          " 
    + "          " + "          " + "          " + "          " + "          " + "          " 
    + "          " + "          " + "     ddddd" + "dddddddddddddddddd"
    + "          " + "          " + "aaaaaaaaaaaaaaaaaaaa" + "       aaa" + "ddddddd"
    + "          " + "          " + "ddd       " + "          " + "          " + "          "
    + "          " + "          " + "          " + "dd        " + "          " + "          "
    + "          " + "          " + "          " + "          " + "          " + "        aa"
    + "          " + "          " + "          " + "          " + "          " + "          "
      "          " + "          " + "          " + "          " + "dddddddddd" + "dddd      "
    + "          " + "          " + "          " + "          " + "          " + "          " 
    + "          " + "          " + "          " + "          " + "          " + "          "
    + "aaaaaaaaaa" + "aa        " + "          " + "          " + "          " + "          "
    + "aaaaaaaaaa" + "dd"
    + "          " + "          " + "          " + "          " + "          " + "          "
    + "d         " + "          " + "        dd" + "dddddddddd" + "dddddd    " + "          "
    + "        aa" + "aaaaaaaaaa" + "aaaaaaaaaa" + "aaaaa"
    + "          " + "          " + "          " + "          " + "          " + "          "
    + "dd        " + "          " + "          " + "          " + "          " + "dddddddddd"
    + "dddddddddd" + "ddddddd   " + "          " + "a         " + "          " + "          "
    + "          " + "          " + "          " + "          " + "          " + "          " 
    + "          " + "          " + "          " + "          " + "          " + "          " 
    + "          " + "d         " + "aaaaaaaaaa" + "aaaaaaaaaa" + "aaaaa" )

while True:
    if len(inp)> 0:
        # a = inp.pop(0)
        a = 0
    else:
        a = input()
    
    if a == 'a':
        game.input_buffer.append(-1)
    elif a == 'd':
        game.input_buffer.append(1)
    else:
        game.input_buffer.append(0)

    state = game.cd()


    tilecomb = {}
    os.system('cls')
    # print(f'Score: {game.output_buffer[2]}')

    for i in range(len(game.output_buffer)//3):
        x = game.output_buffer[i*3]
        if x == -1:
            score = game.output_buffer[i*3+2]
        else:
            y = game.output_buffer[i*3+1]
            tilecomb[(x, y)] = game.output_buffer[i*3+2]
    printd(tilecomb, tileset, False) # print the image
    print(f"Score: {score}")
    if not state:
        break
else:
    print(f'Score: {game.output_buffer[2]}')