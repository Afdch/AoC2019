import os
import re


dw = re.compile(r'deal with increment (\d+)')
cut = re.compile(r'cut (-?\d+)')
shuffle_rules = []
with open(os.path.join(os.getcwd(), r"input", r"d22.txt")) as f:
    for line in f:
        shuffle_rules.append(line.strip())

def modify_rules(shuffle_rules: list) -> list:
    rules = []
    for rule in shuffle_rules:
        incr = dw.match(rule.strip())
        if incr is not None:
            incr = int((incr.group(1)))
            rule = [0, incr]
        elif rule[0:10] == 'deal into ':
            rule = [1]
        elif cut.match(rule):
            cut_no = int((re.search(cut, rule)).group(1))
            rule = [2, cut_no]
        rules.append(rule)
    return rules

def part2_shuffle(number: int, deck_len: int, rules: list, times: int = 1):
    # print(f"Position of {number} card in the {deck_len} cards deck after {times} shuffles")
    position = number
    for i in range(times):
        for rule in list((rules)):
            if rule[0] == 0:
                position = (position * rule[1]) % deck_len            
                # position = (( position * deck_len) // rule[1] ) % deck_len
            elif rule[0] == 1:
                position = deck_len- 1 - position 
            elif rule[0] == 2:
                position -= rule[1]
                if position < 0:
                    position += deck_len
                if position >= deck_len:
                    position -= deck_len

        # print(f"{1+i}\t{position}")        
    return position

rules = modify_rules(shuffle_rules)
m = 119315717514047

def find_a(modulo, c, seed):
    return seed - c if c <= seed < modulo else seed + modulo - c
# 7935, 7105
c = part2_shuffle(0, m, rules)
seed2 = part2_shuffle(1, m, rules)
a = find_a(m, c, seed2)
print( f"multiplier a = {a}, increment c = {c}, modulo = {m}")
# for card in range(m):
#     print(f"Card {card} ends up in position {part2_shuffle(card, m, rules)}")

# times = 101741582076661

def A(times):
    return pow(a, times, m)

def B(times):
    return (c * (1 - A(times)) * pow((1-a) % m, m - 2, m) ) % m

def aF(x, times):
    return ((x - B(times)) * pow(A(times), m - 2, m)) % m

def F(x, times):
    return (A(times) * x + B(times)) % m

search_card = 2020



for times in range(2):
    print(f"Shuffle {times},\tA: {A(times)}, B: {B(times)}")
    print(f"F({search_card}): {F(search_card, times)}")
    print(f"aF(F({search_card})): {aF(F(search_card, times), times)}")
    print(f"shuffle({search_card}): {part2_shuffle(search_card, m, rules, times)}\n")



# as seen, all manual shuffles and calculations are true
# print("After 101741582076661 shuffles the card in position 2020 has number:")
#                                 ## 63088028707442 is too high number
print(aF(2020, 101741582076661)) # 23786834922893 is too low number
# print(aF(2020, 101741582076660)) # 28725567687544 is too low number
# print(aF(2020, 101741582076662)) # 94444582911237 is too high number

# for card in range(10007):
#     part2_shuffle(card)

