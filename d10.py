import os
import math
from PIL import Image

rows, cols = 0, 0
asteroids = {}
with open(os.path.join(os.getcwd(), r"input", r"d10.txt")) as f:
    for line in f:
        cols = 0
        for char in line.strip():
            if char == "#":
                asteroids[cols, rows] = 0
            cols += 1
        rows += 1

# part 1
for ast in asteroids.keys():

    blocked = []
    visible = []
    x, y = ast
    for ast2 in asteroids.keys():
        if ast2 not in ([ast] + blocked + visible):
            x2, y2 = ast2
            dx, dy = x2 - x, y2 - y
            dv = math.gcd(dx, dy)
            dx, dy = dx//dv, dy//dv
            i = 0
            block = False
            while True:
                i += 1
                x1, y1 = x + i * dx, y + i * dy
                if 0 > x1 or x1 > cols or 0 > y1 or y1 > rows:
                    break
                if (x1, y1) in asteroids.keys():
                    if block:
                        blocked.append((x1, y1))
                    else:
                        visible.append((x1, y1))
                        block = True
    asteroids[ast] = len(visible)

chosen = max(asteroids, key=asteroids.get)
print(asteroids[chosen])

# part 2
angles = {}

x, y = chosen
for ast2 in asteroids.keys():
    if ast2 not in [chosen]:
        x2, y2 = ast2
        ang = math.atan2(y2 - y, x2 - x) + math.pi/2
        ang += 2*math.pi if ang < 0 else 0
        if ang not in angles.keys():
            angles[ang] = [ast2]
        else:
            angles[ang].append(ast2)

for angle in angles.keys():
    angles[angle].sort(key = lambda s: abs(s[0] - x) + abs(s[1] - y))

i = 0
last_d = []
while len(angles.keys()) > 0:
    lst = sorted(angles.keys())
    for angle in lst:
        if len(angles[angle]) > 0:
            last_d.append(angles[angle].pop(0))
        if len(angles[angle]) == 0:
            angles.pop(angle, None)
else:
    print(last_d[199][0]*100+last_d[199][1])
    
print("I'm done with this day")