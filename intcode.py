class intcode():
    """
    Intcode computer
    ----------------
    Also intcode is the best thing ever, don't dare to think otherwise
    Provide a command list to the 
    """
    def __init__(self, commands: list, input_buffer: list = [], id: int = 0):
        super().__init__()
        self.input_buffer = input_buffer
        self.output_buffer = []
        self.index = 0           # command index
        self.commands = commands + [0 for _ in range(1000000)]
        self.id = id  
        # provides a number of parameters for each function
        self.instr_param_no = \
            {1: 3, 2: 3, 3: 1, 4: 1, 5: 2, 6: 2, 7: 3, 8: 3, 9:1, 99:0}
        self.relative_base = 0
    
    
    def unwrap(self, opcode: int) -> (int, list):
        """Transforms an integer representing the opcode into the
        tuple containing the modes of parameters.
        Assumes that no more than 3 parameters are used 
        Parameters
        ----------
        opcode: int
            OpCode to convert
        Returns
        -------
        Tuple (int, list)
            Opcode command and a list of mod for command parameters
        """
        op = str(opcode)
        cmnd = int(op[-2:])
        mdf = '{0:0>{1}}'.format(op[:-2], self.instr_param_no[cmnd])
        return (cmnd, list(reversed([int(x) for x in mdf])))

    
    def cd(self) -> bool:
        """
        Follows all the opcodes until it either reaches the end of
        program, or tries to read from the empty input buffer,
        or reads a wrong command from the list
        Returns
        ------- 
        bool
            True if reads an empty input buffer, False if reaches EOP
            or wrong (unsupported) command
        """
        while True:
            instruction = self.unwrap(self.commands[self.index])
            if instruction[0] == 99:
                return False    # program has reached the end

            # get a value for all the elements
            # 0: position mode -- pointer 
            # 1: immediate mode
            # 2: relative mode
            # values = [{
            #     0: self.commands[self.commands[self.index + i + 1]],
            #     1: self.commands[self.index + 1 + i],
            #     2: self.commands[self.commands[self.index + 1 + i] + self.relative_base]
            # }[instruction[1][i]] for i in range(len(instruction[1]))]
            
            values = []
            for i in range(len(instruction[1])):
                a = instruction[1][i]
                if a == 0:
                    value = self.commands[self.commands[self.index + i + 1]]
                elif a == 1:
                    value = self.commands[self.index + 1 + i]
                elif a == 2:
                    value = self.commands[self.commands[self.index + 1 + i] + self.relative_base]
                values.append(value)


            addition = self.relative_base if instruction[1][self.instr_param_no[instruction[0]] - 1] == 2 else 0
            
            #region instructions
            if instruction[0] == 1:
                self.commands[self.commands[self.index + 3] + addition] = values[0] + values[1]
                

            elif instruction[0] == 2:
                self.commands[self.commands[self.index + 3] + addition] = values[0] * values[1]
            elif instruction[0] == 3:
                # input
                if len(self.input_buffer) == 0:
                    return True # program is waiting for input
                else:
                    self.commands[self.commands[self.index + 1] + addition] = self.input_buffer.pop(0)
            elif instruction[0] == 4:
                # output
                self.output_buffer.append(values[0])
            elif instruction[0] == 5:
                self.index = values[1] - 3 if values[0] != 0 else self.index
            elif instruction[0] == 6:
                self.index = values[1] - 3 if values[0] == 0 else self.index
            elif instruction[0] == 7:
                self.commands[self.commands[self.index + 3] + addition] = 1 \
                    if values[0] < values[1] else 0
            elif instruction[0] == 8:
                self.commands[self.commands[self.index + 3] + addition] = 1 \
                    if values[0] == values[1] else 0
            elif instruction[0] == 9:
                self.relative_base += values[0]

            else:
                print("An Error has ocured at index", self.index)
                return False
            #endregion
            self.index += self.instr_param_no[instruction[0]] + 1