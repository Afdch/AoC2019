import os
import timeit

orbits = {}
with open(os.path.join(os.getcwd(), r"input", r"d06.txt")) as f:
    for line in f:
        orb = line.strip().split(')')
        if orb[0] in orbits:
            orbits.update({orb[0]: ([orb[1]] + orbits[orb[0]][0], 0)})
        else:
            orbits[orb[0]] = ([orb[1]], 0)

# part 1
def get_depth(orbit, depth):
    if orbit not in orbits:
        orbits[orbit] = ([], depth)
        return    
    orbits[orbit] = (orbits[orbit][0], depth)
    for children in orbits[orbit][0]:
        get_depth(children, depth + 1)

get_depth('COM', 0)
print(sum([orbits[key][1] for key in orbits.keys()]))
print(orbits)
#part 2

print("done")

def test0():
    get_depth('COM', 0)
    a = (sum([orbits[key][1] for key in orbits.keys()]))
n = 1000
print(n, 'repetitions')
print(timeit.timeit(test0, number=n)/n*1000, 'ms')