import os
import re
from intcode import intcode
import printd
from random import randint

rx = re.compile(r'-?\d+')
with open(os.path.join(os.getcwd(), r"input", r"d15.txt")) as f:
    for line in f:
        opc = [int(x) for x in rx.findall(line)]

directions = {(0, 1): 1,(0, -1): 2,(-1, 0): 3, (1, 0): 4}
directions2 = {1: (0, 1), 2: (0, -1), 3: (-1, 0), 4: (1, 0)}
tileset = {0: ' ', 1: '#', 2: '.', 3: 'D', 4: 'S', 5: 'O'}
colours = {
    0: (0, 0, 0),
    1: (255, 255, 255),
    2: (0, 0, 0),
    3: (255, 0, 0),
    4: (0, 255, 0),
    5: (0, 0, 255)
    }
walker = intcode(opc[:])

tiles = {(0, 0): (3, 0, set([key for key in directions.keys()]))} # (coord) : type of tile, distance from origin, unvisited neighbours
visited = {(0, 0): (4, 0, {0})} 
unvisited = {}
walls = {}
position = ((0, 0), 0)
direction = 1
oxygen_found = False
oxygen = (0, 0)

part1_distance = 0
part2_distance = 0
images = []

while True:
    # region enter new tile -> add neighbours
    neighbors = [tuple([sum(x) for x in \
        zip(position[0], directions2[direction])]) \
            for direction in directions2.keys()]
    for nei in neighbors:
        if nei not in visited:
            if nei not in walls:
                if nei not in unvisited:
                    unvisited[nei] = (0, position[1] + 1, set([position[0]]))
                else:
                    typ, dist, neighs = unvisited[nei]
                    unvisited[nei] = \
                        (typ, min(position[1], dist), neighs|set([position[0]]))
    # endregion
    if len(unvisited) == 0:
        break
    # choose target:
    target_key = list(unvisited)[-1]
    target = unvisited.pop(target_key, None)

    if target is not None:
        if target_key not in neighbors:
            unvisited[target_key] = target
            # TODO backtrack to the neighbor of the target
            ns = set(neighbors)&set(unvisited)
            if len(ns) > 0:
                target_key = ns.pop()
                target = unvisited.pop(target_key, None)
                
            # print("STOP")
            else:
                for key in neighbors:
                    if key not in walls:
                        if visited[key][1] == position[1] - 1:
                            target, target_key = visited[key], key
                            break
    
    direction = directions[tuple(x - y for x, y in zip(target_key, position[0]))]

    walker.input_buffer = [direction]
    
    
    walker.cd()

    # region walker output
    output = walker.output_buffer.pop(0)
    d = tuple([sum(x) for x in zip(position[0], directions2[direction])])
    if output == 2 and not oxygen_found:
        oxygen_found = True
        tiles[position[0]] = (2, 0, neighbors)
        if d in visited:
            distance = visited[d][1]
        else:
            visited[d] = position[0], position[1] + 1, neighbors
            distance = position[1] + 1
        part1_distance = distance
        
        # part 2
        # this position is a new "source"
        # path the maze
        distance = 0
        oxygen = d
        position = (d, distance)
        visited.clear()
        visited[d] = position[0], position[1] + 1, neighbors
        unvisited.clear()
        walls.clear()
        tiles.clear()

        continue
    elif output == 0:
        tiles[d] = (1, 0, 0)
        walls[d] = target
        visited[d] = target
    elif output == 1 or (output == 2 and oxygen_found):
        tiles[position[0]] = (2, 0, neighbors)
        if d in visited:
            distance = visited[d][1]
        else:
            visited[d] = position[0], position[1] + 1, neighbors
            distance = position[1] + 1
        position = (d, distance)
    tls = {key: tiles[key][0] for key in tiles.keys()}
    images.append(printd.mkimg(tls, 8, True, colours,(41, 41)))

printd.savegif(images, 'd15')

# os.system('cls')

if oxygen_found:
    tls[(oxygen)] = 5
tls[(0, 0)] = 4
tls[position[0]] = 3
printd.printd(tls, tileset)


    # endregion

print(part1_distance)
part2_distance = max([visited[key][1] for key in visited.keys() if key not in walls])

print(part2_distance)

printd.imgd(tls, True, 'd15', 8, True, colours, (41, 41))