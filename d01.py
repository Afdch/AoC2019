import os
import math

sum1 = 0
sum2 = 0
with open(os.path.join(os.getcwd(), r"input\d01.txt")) as f:
    for line in f:        
       fuel = int(line.strip())
       # part 1:
       sum1 += math.floor(fuel/3) - 2

       # part 2:
       fuel = int(line.strip())
       while math.floor(fuel/3) - 2 > 0:
           sum2 += math.floor(fuel/3) - 2
           fuel = math.floor(fuel/3) - 2
print(sum1, sum2)