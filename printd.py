tiles = {0: ' ', 1: '█', 2: '■', 3: 'T', 4: 'O'}
colours = {
    0: (0, 0, 0),
    1: (255, 255, 255),
    2: (255, 0, 0),
    3: (0, 255, 0),
    }

def printd(dic: dict, tileset: dict = tiles, reverse: bool = True):
    """Prints a string representative of dots in the terminal
    Parameters
    ----------
    dic: dict
        dictionary with keys in a format of tuple (x, y) coodrdinates
        and integer value for tile

    tileset: dict
        a custom tileset

    reverse: bool
        render *y* coordinate reversed
    """
    X = []
    Y = []
    for tile in dic.keys():
        X.append(tile[0])
        Y.append(tile[1])

    min_x = min(X)
    max_x = max(X) - min(X) + 1
    min_y = min(Y) 
    max_y = max(Y) - min(Y) + 1

    a = [[0 for _ in range(max_x)] for _ in range(max_y)]

    for tile in dic.keys():
        if dic[tile] != None:
            a[tile[1] - min_y][tile[0] - min_x] = dic[tile]

    # print(a)
    lines = []
    for line in a:
        nl = ""
        for char in line:
            c = tileset[char]
            nl += c
        lines.append(nl)
    if reverse:
        lines = reversed(lines)
    for line in lines:
        print(line)

def imgd(dic: dict, save: bool = False, filename: str = 'test.png', \
    scalefactor: int = 1, reverse: bool = True, colourscheme: dict = colours, \
        size: tuple = None):
    """makes an image out of the provided dictionary and saves it in the
    /output/ directory; default filename is "test"; or displays it depending
    on the mode
    Parameters
    ----------
    dic: dict
        dictionary with keys in a format of tuple (x, y) coodrdinates
        and integer value indicating the colour to print

    save: bool
        True: save image
        False: show image

    filename: str
        string indicating the filename 

    scalefactor: int
        scale image by this factor

    reverse: bool
        render _y_ coordinate reversed

    colourscheme: dict
        dictionary with keys as integer and a RGB scheme in integer fromat
        (from 0 to 255)

    size: tuple
        uses user-defined size for the purpose of rendering image
    """
    
    image = mkimg(dic, scalefactor, reverse, colourscheme, size)

    # output?
    if save == 0:
        image.show()
    else:
        import os
        filepath = os.path.join(os.getcwd(), r"output", f"{filename}.png")
        image.save(filepath)

def mkimg(dic: dict, scalefactor: int = 1, reverse: bool = True, \
    colourscheme: dict = colours, size: tuple = None):
    """Returns an image with out of the provided dictionary
    Parameters
    ----------
    dic: dict
        dictionary with keys in a format of tuple (x, y) coodrdinates
        and integer value indicating the colour to print

    save: bool
        True: save image
        False: show image

    filename: str
        string indicating the filename 

    scalefactor: int
        scale image by this factor

    reverse: bool
        render _y_ coordinate reversed

    colourscheme: dict
        dictionary with keys as integer and a RGB scheme in integer fromat
        (from 0 to 255)

    size: tuple
        uses user-defined size for the purpose of rendering image
    
    Return
    ------
    image: Image
    """
    from PIL import Image
    X = []
    Y = []
    for tile in dic.keys():
        X.append(tile[0])
        Y.append(tile[1])

    min_x = min(X)
    max_x = max(X) - min(X) + 1
    min_y = min(Y) 
    max_y = max(Y) - min(Y) + 1
    if size == None:
        size = (max_x, max_y)

    image = Image.new('RGB', size)
    pixels = image.load()

    for x, y in dic.keys():
        if reverse:
            pixels[x - min_x, max_y - (y - min_y) - 1] = colourscheme[dic[(x, y)]]
        else:
            pixels[x - min_x, y - min_y] = colourscheme[dic[(x, y)]]
    # resize image?
    if scalefactor != 1:
        image = Image.Image.resize(image, (size[0]*scalefactor, size[1]*scalefactor))

    return image

def savegif(images: list, filename: str):
    import os
    filepath = os.path.join(os.getcwd(), r"output", f"{filename}.gif")
    images[0].save(filepath, save_all = True, append_images = images[1:], duration = 1, loop = 0)

