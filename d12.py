import os
import re
import numpy as np
from intcode import intcode
from printd import printd, imgd

class moon():
    def __init__(self, position):
        self.position = position
        self.velocity = np.array([0, 0, 0])
        self.acceleration = np.array([0, 0, 0])

objects = []
rx = re.compile(r'-?\d+')
with open(os.path.join(os.getcwd(), r"input", r"d12.txt")) as f:
    for line in f:
        # position, acceleration
        
        objects.append(moon(np.array([int(x) for x in rx.findall(line)])))



    
def change_vel(objs: list) -> list:
    for moon in objs:
        for moon2 in objs:
            if moon2 is not moon:
                for coord in range(3):
                    if moon.position[coord] > moon2.position[coord]:
                        moon.acceleration[coord] -= 1
                    elif moon.position[coord] < moon2.position[coord]:
                        moon.acceleration[coord] += 1
    for moon in objs:
        moon.velocity += moon.acceleration
        moon.position += moon.velocity
        moon.acceleration = np.array([0, 0, 0])

def calc_energy(objs: list):
    energy = 0
    for moon in objs:
        kinetic = sum([abs(x) for x in moon.velocity])
        potent = sum([abs(x) for x in moon.position])
        energy += kinetic * potent
    return energy



# region part 1
for i in range(1000):
    change_vel(objects)
else:
    print(' ')

print(calc_energy(objects))

# region part 2

# from time import time

# time1 = time()
# iterations = 1
# p0 = [np.copy(objects[i].position) for i in range(len(objects))]
# v0 = [np.copy(objects[i].velocity) for i in range(len(objects))]
# b_p = [True for _ in range(4)]
# b_v = [True for _ in range(4)]
# i_p = [0 for _ in range(4)]
# i_v = [[] for _ in range(4)]



# # while (any(b_p)) or ( any(b_v)):    
# while (any([len(vel) < 3 for vel in i_v])):    
#     change_vel(objects)
#     iterations += 1
#     for i in range(4):
#         # if all(p0[i] == objects[i].position):
#         #     if (b_p[i]):
#         #         b_p[i] = False
#         #         i_p[i] = iterations
#         #     print(f"\nposition {i} at {iterations}")

#         if all(v0[i] == objects[i].velocity):
#             # if (b_v[i]):
#             b_v[i] = False
#             i_v[i].append(iterations)
#             # print(f"\nvelocity {i} at {iterations}")
        
#     print(f"\r{time()- time1}\t{iterations}\t"
#     # + f"{b_p}\t{b_v}\t{i_p}\t{i_v}", end="")
#     + f"", end = "")
# print("\nResult")
# [print(vel) for vel in i_v]

# endregion