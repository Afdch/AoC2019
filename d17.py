import os
import re
from intcode import intcode
import printd
import numpy as np

rx = re.compile(r'-?\d+')
with open(os.path.join(os.getcwd(), r"input", r"d17.txt")) as f:
    for line in f:
        opc = [int(x) for x in rx.findall(line)]

def mk_maze(buffer: list):
    i, j = 0, 0
    maze = {}
    # maze = np.array([[ord(x) for x in line] for line in ("".join([str(chr(x)) for x in robot.output_buffer])).strip().split("\n")])
    max_i, max_j = 0, 0
    for ch in buffer:
        if ch == 10:
            j += 1
            i = 0
            max_j = j if j > max_j else max_j
        else:
            maze[(i, j)] = ch
            i += 1
            max_i = i if i > max_i else max_i
            
    tileset = {val: chr(val) for val in maze.values()}
    tileset[0] = ''
    printd.printd(maze, tileset, False)
    max_j -= 1
    return maze, max_i, max_j

def part_1():
    robot = intcode(opc[:])
    robot.cd()
    maze, max_i, max_j = mk_maze(robot.output_buffer)

    alignment_sum = 0
    for i in range(1, max_i - 1):
        for j in range(1, max_j - 1):
            if maze[i, j] == 35:
                if all([maze[i-1, j] == 35, 
                        maze[i+1, j] == 35, 
                        maze[i, j-1] == 35, 
                        maze[i, j+1] == 35]):
                    alignment_sum += i*j
    print(alignment_sum)

def part_2():

    path = "C,B,B,C,A,C,A,C,A,B\n"

    variant_A = "R,12,L,12,L,4,L,4\n"
    variant_B = "R,8,R,12,L,12\n"
    variant_C = "L,6,R,12,R,8\n"

    output = 'n\n'

    input_string = path + variant_A + variant_B + variant_C + output

    opc[0] = 2
    robot = intcode(opc[:])
    robot.input_buffer = list(ord(x) for x in input_string)
    robot.cd()

    mk_maze(robot.output_buffer[:-1])
    print(robot.output_buffer[-1])

part_2()