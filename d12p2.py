import os
import re
import numpy as np
from math import gcd

POS = []
rx = re.compile(r'-?\d+')
with open(os.path.join(os.getcwd(), r"input", r"d12.txt")) as f:
    for line in f:        
        POS.append([int(x) for x in rx.findall(line)])
POS = np.array(POS)

VEL = np.zeros((4,3), dtype= int)

def COUNT(POS, VEL):
    ACC = np.zeros((4,3), dtype= int)
    I, J = POS.shape    
    for i in range(I):
        for j in range(J):
            ACC[i, j] += (POS[:,j] > POS[i, j]).sum()
            ACC[i, j] -= (POS[:,j] < POS[i, j]).sum()
    VEL += ACC
    POS += VEL
    return POS, VEL

incr = 0
# X  = np.copy(POS[:,0])
# Y  = np.copy(POS[:,1])
# Z  = np.copy(POS[:,2])
vX = np.copy(VEL[:,0])
vY = np.copy(VEL[:,1])
vZ = np.copy(VEL[:,2])
# bX, bY, bZ, bvX, bvY, bvZ = [True for _ in range(6)]
bvX, bvY, bvZ = [True for _ in range(3)]

vals = []
# TODO: actually make detection of a cycle
# while any([bX, bY, bZ, bvX, bvY, bvZ]):
while any([bvX, bvY, bvZ]):
    COUNT(POS, VEL)
    incr += 1
    # if all(POS[:,0] == X) and  bX:
    #     bX = False
    #     # print(f"X,\t{incr}")
    # if all(POS[:,1] == Y) and  bY:
    #     bY = False
    #     # print(f"Y,\t{incr}")
    # if all(POS[:,2] == Z) and  bZ:
    #     bZ = False
    #     # print(f"Z,\t{incr}")

    if all(VEL[:,0] == vX) and bvX:
        bvX = False
        print(f"vX,\t{incr}")
        vals.append(incr)
    if all(VEL[:,1] == vY) and bvY:
        bvY = False
        print(f"vY,\t{incr}")
        vals.append(incr)
    if all(VEL[:,2] == vZ) and bvZ:
        bvZ = False
        print(f"vZ,\t{incr}")
        vals.append(incr)

a, b, c = vals
d = a * c // gcd(a, c)
e = d * b // gcd(d, b)
print(e * 2) # magic