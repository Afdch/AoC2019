import os
import re
from intcode import intcode
from printd import printd, imgd

rx = re.compile(r'-?\d+')
with open(os.path.join(os.getcwd(), r"input", r"d11.txt")) as f:
    for line in f:
        opc = [int(x) for x in rx.findall(line)]


painter = intcode(opc[:])

painted = {(0,0): 1} # starting tile; 0 for part 1, 1 for part 2

direction = 1j
tile = (0, 0)

while True:
    # First, it will output a value indicating the color to paint 
    # the panel the robot is over: 0 means to paint the panel black, 
    # and 1 means to paint the panel white.
    # Second, it will output a value indicating the direction 
    # the robot should turn: 0 means it should turn left 90 degrees, 
    # and 1 means it should turn right 90 degrees.
    if tile in painted.keys():
        colour = painted[tile]
    else:
        colour = 0
    painter.input_buffer = [colour]
    program_runs = painter.cd()
    
    if not program_runs:
        break
    
    painted[tile] = painter.output_buffer.pop(0)
    if painter.output_buffer.pop(0) == 0:
        direction = direction * 1j
    else:
        direction = direction / 1j
    tile = (tile[0] + int(direction.real), tile[1] + int(direction.imag))
else:
    print('done!')

print(len(painted.keys()))

# printd(painted)
# imgd(painted)
imgd(painted, True, 'd11', 10)