import os
import re
from intcode import intcode
from itertools import combinations
import networkx as nx
import matplotlib.pyplot as plt

rx = re.compile(r'-?\d+')
room = re.compile(r'- ([a-z ]+)')
nm = re.compile(r'== ([a-zA-Z ]+) ==')
with open(os.path.join(os.getcwd(), r"input", r"d25.txt")) as f:
    for line in f:
        opc = [int(x) for x in rx.findall(line)]

bot = intcode(opc[:])

ship = nx.DiGraph()

reverse = {
    'south': 'north',
    'east': 'west',
    'north': 'south',
    'west': 'east'
}

ignore_items = set(['infinite loop', 'molten lava', 
    'photons', 'giant electromagnet', 'escape pod'])


def part1():
    visited_all = False
    have_items = set()
    last_node = None
    last_direction = None
    bot.cd()
    command = ""
    prepare = True
    while True:
        os.system('cls')
        st = "".join([chr(x) if x < 256 else x for x in bot.output_buffer])
        print(st)
        if command[:4] != 'take' and command[:4] != "drop" and command != 'inv':
            strings = room.findall(st)
            dirs = [x for x in strings if x in reverse.values()] # where can bot go in this room
            items = [x for x in strings if x not in dirs]           # what items are here
            this_node = nm.findall(st)[0]                                # room this_node
        # elif command == 'inv':
        #     have_items == []

        if not visited_all:
            # create a node and edges for the ship graph
            if this_node not in ship.nodes():
                ship.add_node(this_node, direct = {dirk: False for dirk in dirs})
                if last_direction != None:
                    ship.nodes[this_node]['direct'][reverse[last_direction]] = True
                if last_node != None:
                    if last_node not in ship[this_node].keys():
                        ship.add_edge(last_node, this_node, direction = last_direction)
                        ship.nodes[last_node]['direct'][last_direction] = True
                        ship.add_edge(this_node, last_node, direction = reverse[last_direction])
                        ship.nodes[this_node]['direct'][reverse[last_direction]] = True
            
            # print(f"Name = {this_node}\nnodes = {dirs}, items = {items}")
            # print(f"{ship.nodes()} and {ship.edges()}")  

            
            
            

            
            unvisited = {node: [direction for direction in ship.nodes[node]['direct'].keys() \
                    if ship.nodes[node]['direct'][direction] == False] for node in ship.nodes()}
            unvisited = {k: unvisited[k] for k in unvisited.keys() if len(unvisited[k]) > 0}
            if 'Security Checkpoint' in unvisited.keys():
                unvisited.pop('Security Checkpoint')
                    
            paths = [nx.shortest_path(ship, this_node, that_node) for that_node \
                in unvisited.keys() if this_node != that_node]
            # chosen_path = list(sorted(paths))[0]
            # sending a command to a bot
            pick_items = [item for item in items if item not in ignore_items|have_items] if len(items) > 0 else []
            command = ""
            if len(pick_items) > 0:
                command = "take " + pick_items[0]
                have_items.add(pick_items[0])
            elif len(unvisited) > 0:
                if this_node in unvisited and len(unvisited[this_node]) > 0:
                    command = unvisited[this_node].pop()
                else:
                    path = nx.shortest_path(ship, this_node, list(unvisited)[0])
                    dest = path[1]
                    command = ship[this_node][dest]['direction']
            elif len(unvisited) == 0 and this_node != 'Security Checkpoint':
                path = nx.shortest_path(ship, this_node, 'Security Checkpoint')
                dest = path[1]
                command = ship[this_node][dest]['direction']
            elif len(unvisited) == 0 and this_node == 'Security Checkpoint':
                visited_all = True
                combs = []
                for i in range(len(have_items)):
                    combs.extend(combinations(have_items, i))
                combs.extend([tuple(have_items)])
                pos = nx.spring_layout(ship)
                nx.draw(ship, with_labels = True, pos=nx.drawing.layout.spring_layout(ship))
                # nx.draw_networkx_labels(ship, pos)
                plt.show() 
                command = 'east'
            else:
                command = input()

        else:
            os.system('cls')
            if this_node == 'Security Checkpoint':
                if prepare:
                    comb = combs.pop()
                    prepare = False
                    command = 'inv'
                    
                else:
                    dropdn = [item for item in have_items if item not in comb]
                    pickup = [item for item in comb if item not in have_items]
                    if len(dropdn) > 0:
                        item = dropdn.pop()
                        command = 'drop ' + item
                        have_items.remove(item)
                        
                    elif len(pickup) > 0:
                        item = pickup.pop()
                        command = 'take ' + item
                        have_items.add(item)
                        
                    else:
                        command = 'east'
                        prepare = True
                


        if command == 'exit':
            break
        if command in reverse.keys():
            last_node = this_node
            last_direction = command

        bot.input_buffer = [ord(x) for x in command] + [10]
        bot.output_buffer.clear()
        # executing a command a getting info from a bot
        bot.cd()
    pos = nx.spring_layout(ship)
    nx.draw(ship, with_labels = True, pos=nx.drawing.layout.planar_layout(ship))
    # nx.draw_networkx_labels(ship, pos)
    plt.show() 
part1()