import os
import re
from intcode import intcode

rx = re.compile(r'-?\d+')
with open(os.path.join(os.getcwd(), r"input", r"d09.txt")) as f:
    for line in f:
        opc = [int(x) for x in rx.findall(line)]

# day 5 setup
def day9(tmt = True):
    # part 1
    INTCDCMP = intcode(opc[:], [1])
    INTCDCMP.cd()
    if not tmt:
        print(INTCDCMP.output_buffer)
    # part 2
    INTCDCMP = intcode(opc[:], [2])
    INTCDCMP.cd()
    if not tmt:
        print(INTCDCMP.output_buffer)

from timeit import timeit
n = 1
print(n, 'repetitions')
print(timeit(day9, number=n)/n*1000, 'ms')
day9(False)