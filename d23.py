import os
import re
from intcode import intcode

rx = re.compile(r'-?\d+')
with open(os.path.join(os.getcwd(), r"input", r"d23.txt")) as f:
    for line in f:
        opc = [int(x) for x in rx.findall(line)]

network = {address: intcode(opc[:], [address], address) for address in range(50)}


def tick():
    for i in range(50):
        network[i].cd()
        while len(network[i].output_buffer) > 2:
            adr = network[i].output_buffer.pop(0)
            x = network[i].output_buffer.pop(0)
            y = network[i].output_buffer.pop(0)
            if adr == 255:
                return x, y
            network[adr].input_buffer.extend([x, y])
    for i in range(50):
        if len(network[i].input_buffer) == 0:
            network[i].input_buffer.extend([-1])
    return False

def part1():
    while True:
        if (ret := tick()) != False:
            print (ret[1])
            break

def part2():
    last_y, x, y = [None]*3
    while True:
        ret = tick()
        if ret is not False:
            x, y = ret
        if y != None and all([network[i].input_buffer == [-1] for i in range(50)]):
            print(x, y)
            if y == last_y:
                print(y)
                break
            network[0].input_buffer.extend([x, y])
            last_y = y

part1()