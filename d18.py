import os
import re
from intcode import intcode
import printd
import numpy as np

maze = {}
things = {}
with open(os.path.join(os.getcwd(), r"input", r"d18.txt")) as f:
    j = 0
    for line in f:
        i = 0
        for char in line.strip():
            maze[(i, j)] = char
            if char != '#' and char != '.':
                things[char] = (i, j)
            i += 1
        j += 1

print(i, j)

tileset = {c: c for c in maze.values()}
doors = set([obj for obj in things.keys() if (ord(obj) >= 65 and ord(obj) <= 90)])
dkeys = set([obj for obj in things.keys() if (ord(obj) >= 90 and ord(obj) <= 122)])


printd.printd(maze, tileset, False)
#print(things)

keys = {key: [x for x in maze.keys() if maze[x] == key][0] for key in dkeys|set('@')}

def tile_neighs(tile):
    i, j = tile
    return [(x, y) for x, y in [(i+1,j), (i-1,j), (i,j+1), (i,j-1)] if maze[(x, y)] != '#']

def find_path(source: tuple, destination: tuple):
    visited = {keys[source]: (set(), 0)}
    frontier = [(tile, set(), 1) for tile in tile_neighs(keys[source])]
    drs = set()

    while len(frontier) > 0:
        new_door = set()
        step, drs, dist = frontier.pop()
        if step == keys[destination]:
            return drs, dist
        if step in visited.keys():
            if dist < visited[step][1]:
                visited[step][1] = dist
            else:
                continue
        if maze[step] == '#':
            continue
        elif maze[step] in doors:
            # TODO
            new_door = set(chr(ord(maze[step]) + 32))
        frontier.extend([(tile, drs|new_door, dist + 1) for tile in tile_neighs(step)])
        visited[step] = drs|new_door, dist 
    else:
        return False

edges = dict()

for key in keys.items():
    for key2 in keys.items():
        if key != key2:
            edge = find_path(key[0], key2[0])
            if edge != False:
                if key[0] not in edges.keys():
                    edges[key[0]] = dict()
                edges[key[0]][key2[0]] = (edge[0], edge[1])

for edge in edges.keys():
    for e in edges[edge].keys():
        print(f"{edge} => {e}: {edges[edge][e]}")

"""
find the closest point to the starting position
check if there's a valid path"""


def find_path_edges():
    visited = {'@': 0}
    frontier = [(vertice, set(vertice), edges['@'][vertice][1]) \
        for vertice in edges['@'].keys() if edges['@'][vertice][0] == set()]
    
    max_dist = 9999999999

    while len(frontier) > 0:
        if all([el[2] >= max_dist for el in frontier]):
            return max_dist
        vertice, have_keys, dist = frontier.pop()
        # if set(vertice) <= have_keys:
        #     continue
        for vrtc in edges[vertice].keys():
            if vrtc not in visited.keys():
                if edges[vertice][vrtc][0] <= have_keys:        
                    frontier.append((vrtc, have_keys|set(vrtc), dist + edges[vertice][vrtc][1]))
        visited[vertice] = have_keys, dist

        if have_keys == dkeys:
            if max_dist > dist:
                max_dist = dist
                frontier = [(a, b, c) for a, b, c in frontier if c < max_dist]
            continue
    else:
        return max_dist



print(find_path_edges())