import os
import re

rules = {}
fuel = {}
rx = re.compile(r'\d+ [A-Z]+')
with open(os.path.join(os.getcwd(), r"input", r"d14.txt")) as f:
    for line in f:
        rule = [(int(x), el) for x, el in [x.split(' ') for x in [z for z in rx.findall(line)]]]
        for i in range(len(rule) - 1):
            val, el = rule[i]
            val = - val
            rule[i] = (val, el)
        rules[rule[-1][1]] = rule

def add_element(element: str):
    for rule in rules[element]:
        value, elem = rule
        if elem not in fuel:
            fuel[elem] = value
        else:
            fuel[elem] += value

def add_fuel():
    add_element('FUEL')
    while len(OBJ := [x for x in fuel.keys() if (fuel[x] < 0 and x != 'ORE')]) > 0:
        for element in OBJ:
            add_element(element)

def part1():
    add_fuel()
    print(-fuel["ORE"])

def part2_take2():
    global fuel
    rep = 10000
    for i in range(rep):
        add_fuel()
        print(f"\rPreparing: {100*i/rep:>6.2f}%\t", end = "")
    ORE_PER_FUEL = - fuel['ORE']
    repeats = 1000000000000 // ORE_PER_FUEL
    fuel = {key: fuel[key] * (repeats - 1) for key in fuel.keys()}
    print("\rPreparing DONE.\t\t\t\nCalculating FUEL")
    while fuel['ORE'] > - 1000000000000:
        add_fuel()
        if fuel["FUEL"] % 100 == 0:
            print(f'\rFuel: {fuel["FUEL"]:>10}  |  Ore depleted: {-fuel["ORE"]/10000000000:>7.4f}%', end = '')
    print(f'\rFuel: {fuel["FUEL"]:>10}  |  Ore: {-fuel["ORE"]:>13}\t{[fuel[x] for x in fuel.keys() if (x != "ORE" or x != "FUEL")]}')

# part1()
part2_take2()
print(fuel['FUEL'] - 1)