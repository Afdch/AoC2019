# d4 solution using z3 solver
# takes big amount of time tho
from z3 import *

iv = IntVector('iv', 6)
value = Int('value')
s = Solver()
s.add([And(iv[i] >= 0, iv[i] <= 9) for i in range(6)])

s.add(iv[0] <= iv[1], iv[1] <= iv[2], iv[2] <= iv[3], iv[3] <= iv[4], iv[4] <= iv[5])

s.add(Or(And(iv[0] == iv[1], iv[1] != iv[2]), 
         And(iv[0] != iv[1], iv[1] == iv[2], iv[2] != iv[3]),
         And(iv[1] != iv[2], iv[2] == iv[3], iv[3] != iv[4]),
         And(iv[2] != iv[3], iv[3] == iv[4], iv[4] != iv[5]),
         And(iv[3] != iv[4], iv[4] == iv[5])))
s.add(value == Sum([10**(5-k) * i for k, i in enumerate(iv)]))
for c in s.assertions():
    print(c)

s.add(value >= 256310)
s.add(value <= 732736)

valids = []

# need to find all solutions
while s.check() == sat:
    m = s.model()
    # print(m[value])
    valids.append(m[value])
    s.add(value != m[value])

print(len(valids))