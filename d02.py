import os
import re

rx = re.compile(r'\d+')
with open(os.path.join(os.getcwd(), r"input", r"d02.txt")) as f:
    for line in f:
        opc = [int(x) for x in rx.findall(line)]


def prog(noun1, verb1, intcd):
    intcd[1] = noun1
    intcd[2] = verb1
    index = 0
    while intcd[index] != 99:
        if intcd[index] == 1:
            intcd[intcd[index + 3]] = intcd[intcd[index + 1]] + intcd[intcd[index + 2]]
        elif intcd[index] == 2:
            intcd[intcd[index + 3]] = intcd[intcd[index + 1]] * intcd[intcd[index + 2]]
        else:
            return False
        index += 4
    return intcd[0]

# part 1
#reading is hard
# opc[1] = 12
# opc[2] = 2
result = prog(12, 2, list(opc))
print(result)

# part 2
for noun in range(100):
    for verb in range(100):
        if prog(noun, verb, opc.copy()) == 19690720:
            print(noun*100 + verb)