import os
import numpy as np

repeats = 10000

with open(os.path.join(os.getcwd(), r"input", r"d16.txt")) as f:
    for line in f:
        ints = np.array([int(x) for x in line] * repeats)
print(len(ints))

offset = int(''.join([str(x) for x in ints[:7]]))
print(offset)

"""
Point 1:
the elements BEFORE the offset don't participate in calculating the offset+ values at all.
All the elements BEFORE offset are nullified by the mask pattern
d1 = f(d1, d2, d3, ... dk, ..., dn, pattern(1))
d2 = f(d2, d3, ... dk, ..., dn, pattern(1))
dk = f(dk, ..., dn, pattern(k))
dn = f(dn, pattern(n))

Point 2: offset > len(input)/2
This means that PATTERN applied will be always 1

kth element = sum from k to n for elements

point 3: for 6 500 000 values, and offset of 5 978 017, it will still take 521 953 values
in the array.

point 4: as all the elements are the same, but 1 more for each previous line,
we should start at nth element and just add the n-1st, then n-2nd and so on

point 5: as there are no negative numbers, abs() is redundand
"""

ints = ints[offset:]
array_len = len(ints)
print(len(ints))

for i in range(100):
    sum = 0    
    ints = list(reversed([(sum := sum + ints[k]) % 10 for k in reversed(range(len(ints)))]))
    print(f'\rApplying {i+1:>3}%', end = "")

offset = 0
print(ints[offset:offset+8])