import os
import re


dw = re.compile(r'deal with increment (\d+)')
cut = re.compile(r'cut (-?\d+)')
shuffle_rules = []
with open(os.path.join(os.getcwd(), r"input", r"d22.txt")) as f:
    for line in f:
        shuffle_rules.append(line.strip())

def part1_shuffle(deck: list, rules: list, times: int = 1):
    for rule in rules:
        incr = dw.match(rule.strip())
        if incr is not None:
            incr = int((incr.group(1)))
            newr = [0 for _ in range(len(deck))]
            i = 0
            for card in deck:
                newr[i] = card
                i = (i + incr) % len(deck)
            deck = newr
        elif rule[0:10] == 'deal into ':
            deck = deck[::-1]
        elif cut.match(rule):
            cut_no = int((re.search(cut, rule)).group(1))
            deck = deck[cut_no:] + deck[:cut_no]
        # print(f'\r{rule} done \t\t\t\t\t', end = '')
    return deck


deck = [x for x in range(10007)]
deck = part1_shuffle(deck, shuffle_rules)
print(deck)
print( [i for i,x in enumerate(deck) if x == 2019])

# part 2
"""
We should keep track of position of card 2020 only
deck has len of 119315717514047
1: deal with inc N
    pos *= N % deck_len

2: deal into
    pos = deck_len - pos
3: cut N
    pos -= N
    if pos < 0:
        pos += deck_len
    if pos > deck_len
        pos -= deck_len
"""
def modify_rules(shuffle_rules: list) -> list:
    rules = []
    for rule in shuffle_rules:
        incr = dw.match(rule.strip())
        if incr is not None:
            incr = int((incr.group(1)))
            rule = [0, incr]
        elif rule[0:10] == 'deal into ':
            rule = [1]
        elif cut.match(rule):
            cut_no = int((re.search(cut, rule)).group(1))
            rule = [2, cut_no]
        rules.append(rule)
    return rules

def part2_shuffle(number: int, deck_len: int, rules: list, times: int = 1):
    print(f"Position of {number} card in the {deck_len} cards deck after {times} shuffles")
    position = number
    for i in range(times):
        for rule in list(reversed(rules)):
            if rule[0] == 0:
                position = position * rule[1] % deck_len            
                # position = (( position * deck_len) // rule[1] ) % deck_len
            elif rule[0] == 1:
                position = deck_len - position - 1
            elif rule[0] == 2:
                position -= rule[1]
                if position < 0:
                    position += deck_len
                if position >= deck_len:
                    position -= deck_len

        print(f"{1+i}\t{position}")        
    return position

rules = modify_rules(shuffle_rules)

"""
seed2 = (a * seed1 + c) % modulo

1: let seed1 = 0 
=> seed2 = c % modulo
=> c = seed2

2: let seed1 = 1 
=> seed2 = (a + c) % modulo
"""

print(part2_shuffle(2019, 10007, rules))
def find_a(modulo, c, seed):
    return seed - c if c <= seed < modulo else seed + modulo - c

modulo = 119315717514047
c = part2_shuffle(0, modulo, rules)
seed2 = part2_shuffle(1, modulo, rules)
a = find_a(modulo, c, seed2)
print( f"multiplier a = {a}, increment c = {c}")

"""
multiplier a = 86363282532050, increment c = 67356704802338
s_1 = (a * s_0 + c) % m
s_2 = (a * s_1 + c) % m =
    (a**2 * s_0 + a* c + c) % m

s_n = [a ** n * s_0 + c * (1 - a**n)/(1-a)] % m =
    [(a**n * s_0) % m + (c*(1-a**n)/(1-a)) % m] % m =
    = [(a**n% m * s_0) % m + (c*(1-a**n)% m / (1-a) % m)] % m

let (a**n) % m = A
B = c * (1 - A)/(1 - a) % m

F(x) = (A * x + B) mod m
aF(x) = (x - B) / A mod m

According to Fermat's little theorem:
(1 - a)^-1 === (1 - a)^(m-2) | mod m
=> B = c * (1 - A) * (1-a)^(m-2) mod m

1 / A = A ^ (m-2) mod m
aF(x) = (x - B) * A^(m-2) mod m

"""
a = 86363282532050
c = 67356704802338
m = 119315717514047
times = 101741582076661+1
A = pow(a, times, m)
print(A)    # 91619805332589
k = (1-a) % m
B = (c * (1 - A) * pow(k, m - 2, m) ) % m
print(B)    # 91518841709606

def aF(x):
    return ((x - B) * pow(A, m - 2, m)) % m

def F(x):
    return (A * x + B) % m

print(F(2020))
print(aF(2020))
print(aF(F(2020)))
# 63088028707442 is too high
# 23786834922893 is too low