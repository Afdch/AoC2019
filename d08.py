import os
import numpy as np

with open(os.path.join(os.getcwd(), r"input", r"d08.txt")) as f:
    for line in f:
        image = [int(x) for x in line.strip()]

width = 25
height = 6

layers = [np.reshape(np.array(image), (height, width)) \
    for image in [image[i*width*height:(i+1)*width*height] \
        for i in range(len(image)//(width*height))]]

# part 1
a = [[np.count_nonzero(image == 0), \
    np.count_nonzero(image == 1)*np.count_nonzero(image == 2)] \
        for image in layers]
print(sorted(a)[0][1])

# part 2
result = np.full((height, width), 2, dtype= int)
for w in range(width):
    for h in range(height):
        for l in range(len(layers)):
            if layers[l][h][w] == 0:
                result[h][w] = 0
                break
            elif layers[l][h][w] == 1:
                result[h][w] = 1
                break
            
print(result)