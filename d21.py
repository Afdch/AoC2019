import os
import re
from intcode import intcode
import printd
import numpy as np

rx = re.compile(r'-?\d+')
with open(os.path.join(os.getcwd(), r"input", r"d21.txt")) as f:
    for line in f:
        opc = [int(x) for x in rx.findall(line)]

jmpbot = intcode(opc[:])
# part 1
# instr_list = "NOT A J\nNOT C T\nAND D T\nOR T J\nWALK\n"

#========== WRONG ===========T======= RIGHT ===========
#
#           ABCDEFGHI        |        ABCDEFGHI     
#        ..@............     |   ....@............  
#          #####.#.#...##### |
#        #####.#.#...#####   |   #####.#.#...##### 
#          #####.#.#.##.#### |     #####.#.#.##.####
#          #####.##.##.#.### |     #####.##.##.#.###
#                            | #####.##.##.#.###
#          #####.#..######## |     #####.#..########
#          #####.####.#..### |#####.####.#..###
#          #####..#..##..### |    #####..#..##..###
#                            |     #####.##.########              
# part 2
instr_list = """NOT A T
    OR T J
    OR B T
    AND C T
    NOT T T
    AND H T
    OR T J
    AND D J
    RUN
    """


jmpbot.input_buffer = [ord(x) for x in instr_list]
jmpbot.cd()

print(''.join([chr(x) if x < 255 else str(x) for x in jmpbot.output_buffer]))