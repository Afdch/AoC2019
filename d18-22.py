import os
import re
from intcode import intcode
from itertools import combinations, permutations
import printd
import networkx as nx
import matplotlib.pyplot as plt

maze = {}
things = {}
with open(os.path.join(os.getcwd(), r"input", r"d18-2.txt")) as f:
    j = 0
    for line in f:
        i = 0
        for char in line.strip():
            maze[(i, j)] = char
            if char != '#' and char != '.':
                things[char] = (i, j)
            i += 1
        j += 1

print(i, j)
edges = dict()

tileset = {c: c for c in maze.values()}
doors = set([obj for obj in things.keys() if (ord(obj) >= 65 and ord(obj) <= 90)])
dkeys = set([obj for obj in things.keys() if (ord(obj) > 90 and ord(obj) <= 122)])
mazex = nx.Graph()
mazex.add_nodes_from(dkeys)

keys = {key: [x for x in maze.keys() if maze[x] == key][0] for key in dkeys|set('@^$&')}


def tile_neighs(tile):
    i, j = tile
    return [(x, y) for x, y in [(i+1,j), (i-1,j), (i,j+1), (i,j-1)] if maze[(x, y)] != '#']
    
def find_reach(source: tuple):
    visited = {keys[source]: (set(), 0)}
    frontier = [(tile, set(), 1) for tile in tile_neighs(keys[source])]
    drs = set()
    reachable = []
    doors = set([obj for obj in things.keys() if (ord(obj) >= 65 and ord(obj) <= 90)])

    while len(frontier) > 0:
        new_door = set()
        step, drs, dist = frontier.pop()
        # if step == keys[destination]:
        #     return drs, dist
        if step in visited.keys():
            if dist < visited[step][1]:
                visited[step] = visited[step][0], dist
            else:
                continue
        if maze[step] == '#':
            continue
        elif maze[step] in doors:
            # TODO
            new_door = set(maze[step])
            # new_door = set(chr(ord(maze[step]) + 32))
        elif maze[step] in keys:
            reachable.append((maze[step], drs, dist))
            continue
        frontier.extend([(tile, drs|new_door, dist + 1) for tile in tile_neighs(step)])
        visited[step] = drs|new_door, dist 
    else:
        return reachable



for node1 in keys.keys():
    for node2, doors, distance in find_reach(node1):
        keydoors = set([chr(ord(n) - 32) for n in (node1, node2)])
        mazex.add_edge(node1, node2, weight= distance, doors="".join(doors-keydoors))
for node1 in nx.all_neighbors(mazex,'@'):
    for node2 in nx.neighbors(mazex,'@'):
        if node2 != node1 and mazex.has_edge(node1, node2):
            if mazex.edges[node1, node2]['weight'] >= \
                mazex.edges[node1, '@']['weight'] + mazex.edges['@', node2]['weight']:
                mazex.remove_edge(node1, node2)

def draw_maze():    
    #part2
    mazex['@']['$']['weight'] = 30
    mazex['@']['&']['weight'] = 30
    mazex['&']['^']['weight'] = 30
    mazex['^']['$']['weight'] = 30


    pos=nx.drawing.layout.kamada_kawai_layout(mazex)
    nx.draw(mazex, with_labels = True, pos=pos, edge_color ='grey')
    edge_doors = nx.get_edge_attributes(mazex,'doors')
    edge_weight = nx.get_edge_attributes(mazex,'weight')
    edge_labels = {k: edge_doors[k] + "|" + str(edge_weight[k]) for k in edge_doors.keys()}


    # edge_labels = nx.get_edge_attributes(mazex,'doors')
    edge_labels = nx.draw_networkx_edge_labels(mazex, edge_labels=edge_labels, pos=pos)
    plt.show() 

draw_maze()

# 3715 is too high
solutions = set()

visited = {}
visit_once = set(['bt', 'x', 'y', 'p', 'q', 'm', 'gj', 'i', 'ugh', 'wcfkasn'])

frontier = [(node, set([node, '@']), mazex['@'][node]['weight']) for node in nx.all_neighbors(mazex, '@')]

while len(frontier) > 0:
    frontier.sort(key=lambda x: len(x[1]))
    curr_node, visited_this_route, dist = frontier.pop()
    if set("".join([x for x in visited_this_route])) >= dkeys:
        solutions.add(dist)
        continue
    for node in nx.all_neighbors(mazex, curr_node):
        if (node in visited_this_route and node in visit_once) or node == '':
            pass
        elif set([chr(ord(x) + 32) for x in [x for x in mazex[curr_node][node]['doors']]]) \
            <= set("".join([x for x in visited_this_route])):
            nxt = (node, visited_this_route|set([node]), dist+mazex[curr_node][node]['weight'])
            if nxt[2] > 3715:
                continue
            if (nxt[0], frozenset(nxt[1])) in visited:
                if visited[(nxt[0], frozenset(nxt[1]))] <= nxt[2]:
                    continue
                else:
                    visited[(nxt[0], frozenset(nxt[1]))] = nxt[2]
                    frontier.append(nxt)
            else:
                frontier.append(nxt)
                visited[(nxt[0], frozenset(nxt[1]))] = nxt[2]
else:
    print(min(solutions))
