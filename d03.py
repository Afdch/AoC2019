import re; wires = [[(x[0], int(x[1:])) for x in re.compile(r'[RULD]\d+').findall(line)] for line in open('input/d03.txt')]; 
def gp(wire):
    x, y, lng, points = 0, 0, 0, {} 
    for direction, length in wire:
        for _ in range(length):
            x, y, lng = x + {'R': (1, 0), 'U': (0, 1), 'L': (-1, 0), 'D': (0, -1)}[direction][0], y + {'R': (1, 0), 'U': (0, 1), 'L': (-1, 0), 'D': (0, -1)}[direction][1], lng + 1; points.update({(x, y): lng}) if (x, y) not in points.keys() else {}
    return points
print(sorted([abs(x) + abs(y) for x, y in gp(wires[0]).keys() & gp(wires[1]).keys()])[0], '\n', sorted([gp(wires[0])[inter] + gp(wires[1])[inter] for inter in gp(wires[0]).keys() & gp(wires[1]).keys()])[0])